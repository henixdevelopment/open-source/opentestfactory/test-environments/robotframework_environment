#!/usr/bin/env bash

log_message() {
    local level=$1
    shift
    echo "[${level}] $*"
}

###### Setting the SSH Configuration ######
# set umask for sftp
UMASK=${UMASK:-022}
sudo sed -i "s|/usr/lib/ssh/sftp-server$|/usr/lib/ssh/sftp-server -u ${UMASK}|g" /etc/ssh/sshd_config

# change port from default if requested
if [ "$SSHD_PORT" != "" ]; then
    sudo sed "s/#Port 22/Port $SSHD_PORT/" -i /etc/ssh/sshd_config
fi

####### User credentials #######

# Set ssh key directory
if [ ! -f /home/otf/.ssh/authorized_keys ];then
    sudo touch /home/otf/.ssh/authorized_keys
fi

# filter out if the user set both a public key and a password
if [ "$USER_PASSWORD" != "" ] && ( [ "$PUBLIC_KEY" != "" ] || [ "$PUBLIC_KEY_FILE" != "" ] ); then 
        log_message "ERROR" "Password and public key are both defined."
        log_message "ERROR" "This is unsupported."
        log_message "ERROR" "Please define only password or public key."
        log_message "ERROR" "Shutting down the container."
        exit 1

# Case 1) Use of a public key
elif [ "$PUBLIC_KEY" != "" ] || [ "$PUBLIC_KEY_FILE" != "" ]; then
   
    # Adding key through CLI (if doesn't already exist)
    if [ "$PUBLIC_KEY" != "" ] && [ ! "$(grep "$PUBLIC_KEY" /home/otf/.ssh/authorized_keys)" ] && [ "$PUBLIC_KEY_FILE" == "" ]; then
        echo "$PUBLIC_KEY" >> /home/otf/.ssh/authorized_keys
        log_message "INFO" "Public key from env variable added"

    # Adding key through file
    elif [ "$PUBLIC_KEY_FILE" != "" ] && [ "$PUBLIC_KEY" == "" ]; then

        # Check if the file is found at the defined path
        if [ ! -e "$PUBLIC_KEY_FILE" ]; then
            log_message "ERROR" "Public key file not found at the path $PUBLIC_KEY_FILE."
            log_message "ERROR" "Be sure that you have mounted a volume with the key file inside and that the path is correct."
            log_message "ERROR" "Shutting down the container."
            exit 1
        fi

        PUBLIC_KEY2=$(cat "$PUBLIC_KEY_FILE") 
        # Check if already exists
        if [ ! "$(grep "$PUBLIC_KEY2" /home/otf/.ssh/authorized_keys)" ]; then
            echo "$PUBLIC_KEY2" >> /home/otf/.ssh/authorized_keys
            log_message "INFO" "Public key from file added"
        fi

    # If PUBLIC_KEY and PUBLIC_KEY_FILE are both set
    else
        log_message "ERROR" "Only one public key can be set."
        log_message "ERROR" "Please define only PUBLIC_KEY or PUBLIC_KEY_FILE."
        log_message "ERROR" "Shutting down the container."
        exit 1
    fi

# Case 2) Use of a password
elif [ "$USER_PASSWORD" != "" ]; then
    # Activate password in sshd_config
    sudo sed "s/PasswordAuthentication no/PasswordAuthentication yes/" -i /etc/ssh/sshd_config

    # Add the password to user otf
    sudo usermod -p $(echo "$USER_PASSWORD" | openssl passwd -stdin) otf
    log_message "INFO" "Password is set"

# Default case if nothing is set
else
    log_message "ERROR" "No USER_PASSWORD=(string), PUBLIC_KEY=(string), nor PUBLIC_KEY_FILE=(path/to/file) defined as environment variable!"
    log_message "ERROR" "Environment cannot run without security on ssh."
    log_message "TIPS" "Use '--env MY_VAR=Value' if you use docker run."
    log_message "ERROR" "Shutting down the container."
    exit 1
fi    

# User is built in and can't be changed
log_message "INFO" "The ssh user of this image is 'otf'."

# Set permisson for otf user
sudo chmod 700 /home/otf/.ssh 
sudo chmod 600 /home/otf/.ssh/authorized_keys
